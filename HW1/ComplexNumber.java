
/**
 *
 * @author tylerfontana
 */
public class ComplexNumber{
    /* The real part of the complex number. */
    protected  float a;
    /* The imaginary part of the complex number. */
    protected  float b;
     /**
    * Constructs a new ComplexNumber with the given real
    * and imaginary parts.
    *
    * @param a The real part.
    * @param b The imaginary part.
    */
    public ComplexNumber(float a, float b){
        this.a = a;
        this.b = b;
    }
     /**
      * Method that gets the real part of the complex number.
      * @return the real part of the complex number
      */
     public float getA(){
        return this.a;
    }
     /**
      * Method that gets the imaginary part of the complex number.
      * @return the imaginary part of the complex number
      */
     public float getB(){
        return this.b;
    }
    /**
    * Returns the result of this ComplexNumber added to
    * another.
    *
    * @param other The ComplexNumber to add.
    * @return The result of this + other.
    */
     public ComplexNumber add(ComplexNumber other){
        float newA = this.a + other.getA();
        float newB = this.b + other.getB();
        return new ComplexNumber(newA, newB);
     }
     /**
    * Returns the result of this ComplexNumber subtracted by
    * another.
    *
    * @param other The ComplexNumber to subtract.
    * @return The result of this - other.
    */
    public ComplexNumber subtract(ComplexNumber other){
        float newA = this.a - other.getA();
        float newB = this.b - other.getB();
        return new ComplexNumber(newA, newB);
    }
    /**
    * Returns the result of this ComplexNumber multiplied to
    * another.
    *
    * @param other The ComplexNumber to multiply.
    * @return The result of this * other.
    */
     public ComplexNumber multiply(ComplexNumber other){
        float newA = (this.a * other.getA()) - (this.b * other.getB());
        float newB = (this.b * other.getB()) + (this.a * other.getA());
        return new ComplexNumber(newA, newB);
    }
    /**
    * Returns the result of this ComplexNumber divided by
    * another.
    *
    * @param other The ComplexNumber to divide.
    * @return The result of this / other.
    */
     public ComplexNumber divide(ComplexNumber other) throws Exception{
        float newA = (this.a * other.getA() + this.b * other.getB()) / (other.getA() * other.getA() + other.getB() * other.getB());
        float newB = (this.b * other.getB() - this.a * other.getB()) / (other.getA() * other.getA() + other.getB() * other.getB());
        return new ComplexNumber(newA, newB);
    } 
    /**
     * {@inheritDoc}
     */
     @Override
     public boolean equals(Object other){
        if (other == null){
            return false;
        }
        else if(!(other instanceof ComplexNumber)){
            return false;
        }
        else{
            ComplexNumber a1 = (ComplexNumber) other;
            return (a1.a == this.a && a1.b == this.b);
        }
    }
    /**
     * Returns the result of the Complex Number in String format.
     * 
     * @return the ComplexNumber
     */
    @Override
    public String toString(){
        String answer = "" + a + " + " + b + "i";
        return answer;
    }
}
