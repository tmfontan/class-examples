/**
 *
 * @author tylerfontana
 */
public class RealNumber extends ComplexNumber{
     /**
     * Creates a new RealNumber with only the real part of the Complex Number.
     *
     * @param a The real part a.
     */
    public RealNumber(float a){
        super(a,0.0f);
    }
    /**
     * Determines if this Real Number is less than another.
     *
     * @param other The other real number that is being compared.
     * @return Whether the RealNumber is less than the other real number.
     */
    public boolean isLessThan(RealNumber other){
        return (this.a < other.a);
    }
     /**
     * Determines if this Real Number is greater than another.
     *
     * @param other The other real number that is being compared.
     * @return Whether the RealNumber is greater than the other real number.
     */
    public boolean isGreaterThan(RealNumber other) {
        return (this.a > other.a);
    }
}
