
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
/**
 * Class Tests the various Methods of ComplexNumber.java
 *
 * @author tylerfontana
 * @version 2/7/14
 */
public class ComplexNumberTest{
    
    //Creates Various Random Value Real Numbers.
    private ComplexNumber numA;
    private ComplexNumber numB;
    private ComplexNumber numC;
    private ComplexNumber numD;
    
    //Creates four ComplexNumbers to be used as test scenarios.
    @Before
    public void setUp() {
        numA = new ComplexNumber(1, 2); 
        numB = new ComplexNumber(7, 6); 
        numC = new ComplexNumber(5, 16); 
        numD = new ComplexNumber(0, 8); 
    }
    
    //Tests the Add method.
    @Test 
	public void testAdd() {
		ComplexNumber num1 = numA.add(numB);
        assertEquals(8, num1.getA());
        assertEquals(8, num1.getB());
        ComplexNumber num2 = numC.add(numD);
        assertEquals(5, num2.getA());
        assertEquals(24, num2.getB());
        ComplexNumber num3 = numA.add(numC);
        assertEquals(6, num3.getA());
        assertEquals(18, num3.getB());
        ComplexNumber num4 = numB.add(numD);
        assertEquals(7, num4.getA());
        assertEquals(14, num4.getB());
        }
    //Tests the Subtract method.
    @Test 
    public void testSubtract() {
        ComplexNumber num5 = numA.subtract(numB);
        assertEquals(-6, num5.getA());
        assertEquals(-4, num5.getB());
        ComplexNumber num6 = numC.subtract(numD);
        assertEquals(5, num6.getA());
        assertEquals(8, num6.getB());
        ComplexNumber num7 = numA.subtract(numC);
        assertEquals(-4, num7.getA());
        assertEquals(-14, num7.getB());
        ComplexNumber num8 = numB.subtract(numD);
        assertEquals(7, num8.getA());
        assertEquals(-2, num8.getB());
    }
    //Tests the Multiply method.
    @Test 
    public void testMultiply() {
        ComplexNumber num9 = numA.multiply(numB);
        assertEquals(7, num9.getA());
        assertEquals(12, num9.getB());
        ComplexNumber num10 = numC.multiply(numD);
        assertEquals(0, num10.getA());
        assertEquals(128, num10.getB());
        ComplexNumber num11 = numA.multiply(numC);
        assertEquals(5, num11.getA());
        assertEquals(32, num11.getB());
        ComplexNumber num12 = numB.multiply(numD);
        assertEquals(0, num12.getA());
        assertEquals(48, num12.getB());
    }
    //Tests the Division method.
    @Test 
    public void testDivide() {
        ComplexNumber num13 = numA.divide(numB);
        assertEquals(0.14285715, num13.getA());
        assertEquals(0.33333334, num13.getB());
        ComplexNumber num14 = numC.divide(numD);
        assertEquals(null, num14.getA());
        assertEquals(2, num14.getB());
        ComplexNumber num15 = numA.divide(numC);
        assertEquals(0.2, num15.getA());
        assertEquals(0.125, num15.getB());
        ComplexNumber num16 = numB.divide(numD);
        assertEquals(null, num16.getA());
        assertEquals(0.75, num16.getB());
    }
}