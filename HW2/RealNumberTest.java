import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
/**
 * Class Tests the various Methods of RealNumber.java
 *
 * @author tylerfontana
 * @version 2/7/14
 */
public class RealNumberTest{

	//Creates Various Random Value Real Numbers.
	private RealNumber numA;
	private RealNumber numB;
	private RealNumber numC;
	private RealNumber numD;

	//Creates four RealNumbers to be used as test scenarios.
	@Before
	public void setUp() {
        numA = new RealNumber(1); 
        numB = new RealNumber(7);
        numC = new RealNumber(5); 
        numD = new RealNumber(0); 
    }
    //Tests the isLessThan Method.
	@Test
	public void testIsLessThan() {
		assertEquals(true, numA.isLessThan(numB));
		assertEquals(false, numB.isLessThan(numA));
		assertEquals(false, numC.isLessThan(numD));
		assertEquals(true, numD.isLessThan(numC));
	}
	//Tests the isGreaterThan Method.
	@Test
	public void testIsGreaterThan() {
		assertEquals(false, numA.isGreaterThan(numB));
		assertEquals(true, numB.isGreaterThan(numA));
		assertEquals(true, numC.isGreaterThan(numD));
		assertEquals(false, numD.isGreaterThan(numC));
	}
}