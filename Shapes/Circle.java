/**
 * A circle shape, represented by its radius.
 *
 * @author Nathan Cooper
 */
public class Circle implements Shape {

    /** The radius of the Circle. */
    private double radius;

    /**
     * Constructs a new Circle with the given radius.
     *
     * @param radius The radius to use.
     */
    public Circle(final double radius){
        if (radius <= 0){
            throw new IllegalArgumentException("Invalid radius given.");
        }
        this.radius = radius;
    }

    /**
     * Returns the radius of the Circle.
     *
     * @return The current radius.
     */
    public double getRadius(){
        return this.radius;
    }

    /**
     * Sets the radius of the circle.
     */
    public void setRadius(final double radius){
        if (this.radius <= 0){
            throw new IllegalArgumentException("Invalid radius given.");
        }
       this.radius = radius;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object object){
        if (object == null){
            // If the other object is null, we can't be equal to it.
            return false;
        }
        else if (!(object instanceof Circle)){
            // If the other object isn't a Circle, we can't be equal to it.
            return false;
        }
        else {
            // Otherwise, if we are comparing ourselves to a non-null circle, we are only
            // equal if our radius is equal to the other's radius.
            final Circle other = (Circle) object;
            return (other.radius == this.radius);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        return "I am a circle of radius " + this.radius + ".";
    }

    /**
     * {@inheritDoc}
     *
     * For a circle, the area is PI*r^2.
     */
    @Override
    public double getArea(){
        return Math.PI * this.radius * this.radius;
    }

}
