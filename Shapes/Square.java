/**
 * A square, which a special case of a Rectangle where length == width.
 */
public class Square extends Rectangle {

    /**
     * Builds a new Square with a given edge size.
     *
     * @param side The side length, both the length and the width. Must be positive.
     */
    public Square(final double side){
        // Call the superclass constructor to initialize both length and width.
        super(side,side);
    }

}
