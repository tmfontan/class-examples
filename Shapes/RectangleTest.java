/*
 * Import the JUnit framework. This must be part of our classpath before
 * this import will be recognized.
 */
import org.junit.*;

/*
 * Import each of the static methods in org.junit.Assert, so we can call it as:
 *
 * assertEquals(2,3)
 *
 * instead of
 *
 * Assert.assertEquals(2,3).
 */
import static org.junit.Assert.*;
import org.junit.Test;

public class RectangleTest {

    /** The required precision of our double comparisons. */
    private static final double EPSILON = 0.000000001;

    /** The first TEST FIXTURE. A 2 x 3 Rectangle. */
    private Rectangle rect2x3;

    /** Our second fixture. A 1 x 1 Rectangle. */
    private Rectangle rect1x1;

    /**
     * This is an @Before method -- it will run every time before each actual
     * test method below. Here, we are setting up two fresh test objects
     * each time.
     */
    @Before
    public void setupTextFixtures(){
        this.rect2x3 = new Rectangle(2,3);
        this.rect1x1 = new Rectangle(1,1);
    }

    // Test methods: Each method below is marked with an @Test annotation.

    /** Tests that the constructor produces a valid Rectangle. */
    @Test
    public void testConstructor(){
        assertEquals(2, rect2x3.getLength(),EPSILON);
        assertEquals(3, rect2x3.getWidth(),EPSILON);
        assertEquals(1, rect1x1.getLength(),EPSILON);
        assertEquals(1, rect1x1.getWidth(),EPSILON);
    }

    /** Tests that the getArea() method is correct. */
    @Test
    public void testGetArea(){
        assertEquals(6, rect2x3.getArea(),EPSILON);
        assertEquals(1, rect1x1.getArea(),EPSILON);
        assertTrue(Math.abs(1- rect1x1.getArea())<EPSILON);
    }

    /** Tests that the getPerimeter() method is correct. */
    @Test
    public void testGetPerimeter(){
        assertEquals(10, rect2x3.getPerimeter(),EPSILON);
        assertEquals(4, rect1x1.getPerimeter(),EPSILON);
    }

    /** Tests that the toString() method is valid. */
    @Test
    public void testToString(){
        String expected = "I am a rectangle of width 3.0 and length 2.0.";
        assertEquals(expected, rect2x3.toString());
        expected = "I am a rectangle of width 1.0 and length 1.0.";
        assertEquals(expected, rect1x1.toString());
    }

    /** Tests that equals() is valid. */
    @Test
    public void testEquals(){
        assertFalse(rect2x3.equals(rect1x1));
        assertFalse(rect1x1.equals(rect2x3));
        assertTrue(rect2x3.equals(rect2x3));
        assertTrue(rect1x1.equals(rect1x1));
    }


}
