/**
 * An interface that represents a shape.
 *
 * @author Nathan Cooper
 */
public interface Shape {

    /**
     * Returns the area of the shape. The value of getArea() must
     * always be > 0.
     *
     * @return The area of the shape. Must be > 0.
     */
    double getArea();

}
