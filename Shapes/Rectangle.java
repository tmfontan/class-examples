/**
 * Represents a rectangular Shape.
 *
 * @author Nathan Cooper
 */
public class Rectangle implements Shape {

    /**
     * Returns the length of this Rectangle.
     *
     * @return The length of this Rectangle.
     */
    public double getLength() {
        return this.length;
    }

    /**
     * Returns the width of this Rectangle.
     *
     * @return The width of this Rectangle.
     */
    public double getWidth() {
        return this.width;
    }

    /** The length of this Rectangle. */
    private final double length;

    /** The width of this Rectangle. */
    private final double width;

    /**
     * Constructs a new rectangle. Length and width must both be positive.
     *
     * @param length The length, must be positive.
     * @param width The width, must be positive.
     */
    public Rectangle(final double length, final double width){
        if (length <= 0 || width <= 0){
            /* We have specified in our *contract* above that length and width
             * must both be positive. So technically this check is unnecessary. But we
             * are deciding to be defensive and avoid getting in a bad state.
             */
            throw new IllegalArgumentException("Invalid length or width.");
        }
        this.length = length;
        this.width = width;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object object){
        if (this == object){
            return true;
        }
        else if (object == null){
            // If the other object is null, we can't be equal to it.
            return false;
        }
        else if (!(object instanceof Rectangle)){
            // If the other object isn't a Rectangle, we can't be equal to it.
            return false;
        }
        else {
            // Otherwise, if we are comparing ourselves to a non-null rectangle, we are only
            // equal if our radius is equal to the other's radius.
            final Rectangle other = (Rectangle)object;
            return (other.length == this.length && other.width == other.width);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        return "I am a rectangle of width " + this.width + " and length " + this.length + ".";
    }

    /**
     * {@inheritDoc}
     *
     * The length of a rectangle is its length times its width.
     */
    @Override
    public double getArea() {
        return this.length * this.width;
    }

    /**
     * Returns the perimeter of this rectangle.
     *
     * @return The perimeter of the rectangle.
     */
    public double getPerimeter(){
        return this.length + this.width + this.length + this.width;
    }
}
