/**
 * Can we try to make a Shape with zero area? What would happen?
 */
public class Point implements Shape {

    /**
     * {@inheritDoc}
     *
     * Returns the area of a Point... but wait, that's zero! Our superclass
     * has a post-condition that the area is GREATER than zero. And postconditions
     * can't become looser in the subclass! Therefore, this is technically BAD.
     */
    public double getArea(){
        return 0;
    }

    // For instance...

    /**
     * Returns the average density of a shape given its mass.
     *
     * @param shape The shape to measure.
     * @param mass The mass of the given shape.
     * @return The average area density of the given shape.
     */
    public static double getDensity(final Shape shape, final double mass){
        /* This line is allowed by Shape's contract since getArea() is always > 0...
         * ... but this Point class violates this by loosening this post-condition
         * and allowing zero! So if <code>shape</code> is a Point, this will fail!*/
        return mass / shape.getArea();
    }
}
