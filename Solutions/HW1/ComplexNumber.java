/**
 * A class representing a complex number with both a real part and an imaginary part.
 *
 * @author Nathan Cooper
 */
public class ComplexNumber {

    /** The real part of this ComplexNumber. */
    protected final float a;

    /** The imaginary part of this ComplexNumber. */
    protected final float b;

    /**
     * Constructs a new ComplexNumber with the specified real and imaginary parts.
     *
     * @param a The real part.
     * @param b The imaginary part.
     */
    public ComplexNumber(final float a, final float b){
        this.a = a;
        this.b = b;
    }

    /**
     * Returns the real part of this ComplexNumber.
     *
     * @return The real part, A.
     */
    public float getA(){
        return this.a;
    }

    /**
     * Returns the imaginary part of this ComplexNumber.
     *
     * @return The imaginary part, B.
     */
    public float getB(){
        return this.b;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object object){
        if (!(object instanceof ComplexNumber)){
            return false;
        }
        else {
            final ComplexNumber other = (ComplexNumber)object;
            return (this.a == other.a && this.b == other.b);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString(){
        /*
         * This is fancier than it needs to be. You can just get away with:
         *      return this.a + " + " + this.b + "i";
         */
        if (this.a == 0 || this.b == 0){
            if (this.a != 0){
                // Purely real (e.g. 7 or -7).
                return String.valueOf(this.a);
            }
            else if (this.b != 0){
                // Purely imaginary (e.g. 12i or -12i).
                return String.valueOf(this.b) + "i";
            }
            else {
                // Entirely zero (e.g. 0).
                return "0.0";
            }
        }
        else {
            // Typical case. Both real and imaginary are present.
            // E.g. 12+6i, 12-6i, or -12-12i.
            final String sign = (this.b < 0) ? "" : "+";
            return this.a + sign + this.b + "i";
        }
    }

    /**
     * Returns the sum of this ComplexNumber and another.
     *
     * @param other The right-hand side.
     * @return This ComplexNumber + other.
     */
    public ComplexNumber add(final ComplexNumber other){
        return new ComplexNumber(this.a+other.a,this.b+other.b);
    }

    /**
     * Returns the difference of this ComplexNumber and another.
     *
     * @param other The right-hand side.
     * @return This ComplexNumber - other.
     */
    public ComplexNumber subtract(final ComplexNumber other){
        return new ComplexNumber(this.a-other.a,this.b-other.b);
    }

    /**
     * Returns the product of this ComplexNumber and another.
     *
     * @param other The right-hand side.
     * @return This ComplexNumber * other.
     */
    public ComplexNumber multiply(final ComplexNumber other){
        final float a = this.a * other.a - this.b * other.b;
        final float b = this.b * other.a + this.a * other.b;
        return new ComplexNumber(a,b);
    }

    /**
     * Returns the quotient of this ComplexNumber and another.
     *
     * @param other The right-hand side.
     * @return This ComplexNumber / other.
     * @throws ArithmeticException If other == 0.
     */
    public ComplexNumber divide(final ComplexNumber other){
        if (other.a == 0.0 && other.b == 0.0){
            // We cannot divide by zero.
            throw new ArithmeticException();
        }
        else {
            // Store the common divisor here so we don't have to compute it twice.
            final float d = (other.a * other.a + other.b * other.b);
            // Compute the new real and imaginary parts.
            final float a = (this.a * other.a + this.b * other.b) / d;
            final float b = (this.b * other.a - this.a * other.b) / d;
            return new ComplexNumber(a,b);
        }
    }

}
