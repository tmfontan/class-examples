import java.io.IOException;
import java.lang.IllegalStateException;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Application that lets us view accounts by their type.
 */
public class CreditInquiry {

    private static final MenuOption[] choices = MenuOption.values();

    /**
     * Continually asks the user for an account type and prints the corresponding accounts.
     * @param args Not used.
     */
    public static void main(String[] args){
        MenuOption accountType = getRequest();
        while (accountType != MenuOption.END){
            switch (accountType){
                case ZERO_BALANCE:
                    System.out.println("Accounts with zero balance:");
                    break;
                case CREDIT_BALANCE:
                    System.out.println("Accounts with credit balance:");
                    break;
                case DEBIT_BALANCE:
                    System.out.println("Accounts with debit balance:");
                    break;
            }
            readRecords(accountType);
            accountType = getRequest();
        }
    }

    /**
     * Asks the user for a menu request and returns the answer parsed as a MenuOption.
     *
     * @return The MenuOption chosen.
     */
    private static MenuOption getRequest(){
        int request = 4;
        System.out.println("What do you want?\n1) Zero Balance Accounts\n2) Credit Accounts\n3) Debit Accounts\n4) Exit\n");
        try {
            Scanner input = new Scanner(System.in);
            do {
                System.out.print("Choice: ");
                request = input.nextInt();
            } while (request < 1 || request > 4);
        } catch (NoSuchElementException e){
            System.err.println("Invalid input. Terminating.");
        }
        return choices[request - 1];
    }

    /**
     * Prints all the accounts from clients.txt that match the given MenuOption type.
     *
     * @param accountType Which type of account to use.
     */
    private static void readRecords(MenuOption accountType){

        try {
            Scanner input = new Scanner(Paths.get("clients.txt"));
            while (input.hasNext()){
                Account account = new Account(input.nextInt(),input.next(),input.next(),input.nextDouble());
                if (shouldDisplay(accountType,account)){
                    System.out.printf(
                            "%-10d%-12s%-12s%12.2f%n",
                            account.getAccount(),
                            account.getFirstName(),
                            account.getLastName(),
                            account.getBalance()
                    );
                    System.out.println();
                }
                else {
                    input.nextLine();
                }
            }
        } catch (NoSuchElementException | IOException | IllegalStateException e){
            System.err.println("Error handling file. Terminating.");
            System.exit(1);
        }

    }

    /**
     * Given an Account type, returns whether we should display it based on the MenuOption chosen.
     *
     * @param accountType Which account type we are after.
     * @param account The account to query.
     * @return Whether we should display it.
     */
    private static boolean shouldDisplay(MenuOption accountType, Account account){
        if (accountType == MenuOption.CREDIT_BALANCE && account.getBalance() < 0){
            return true;
        }
        else if (accountType == MenuOption.DEBIT_BALANCE && account.getBalance() > 0){
            return true;
        }
        else if (accountType == MenuOption.ZERO_BALANCE && account.getBalance() == 0){
            return true;
        }
        else {
            return false;
        }
    }

}